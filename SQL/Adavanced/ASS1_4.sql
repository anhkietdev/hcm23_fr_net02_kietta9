-- Insert records into EMPMAJOR
INSERT INTO [dbo].[EMPMAJOR] ([emp_no], [major], [major_name])
VALUES
    ('EMP001', 'M01', 'Computer Science'),
    ('EMP001', 'M02', 'Electrical Engineering'),
    ('EMP002', 'M03', 'Mechanical Engineering'),
    ('EMP003', 'M01', 'Computer Science'),
    ('EMP003', 'M04', 'Civil Engineering'),
    ('EMP004', 'M02', 'Electrical Engineering'),
    ('EMP005', 'M05', 'Chemistry'),
    ('EMP006', 'M06', 'Physics');

 

-- Insert records into EMP
INSERT INTO [dbo].[EMP] ([emp_no], [last_name], [first_name], [dept_no], [job], [salary], [bonus], [ed_level])
VALUES
    ('EMP001', 'Smith', 'John', 'D01', 'Manager', 75000.00, 5000.00, 4),
    ('EMP002', 'Johnson', 'Alice', 'D02', 'Engineer', 65000.00, NULL, 3),
    ('EMP003', 'Williams', 'David', 'D03', 'Analyst', 60000.00, 3000.00, 4),
    ('EMP004', 'Jones', 'Mary', 'D02', 'Engineer', 70000.00, 2500.00, 3),
    ('EMP005', 'Brown', 'Sarah', 'D01', 'Manager', 80000.00, 6000.00, 4),
    ('EMP006', 'Davis', 'Michael', 'D03', 'Analyst', 62000.00, 3500.00, 4),
    ('EMP007', 'Wilson', 'Laura', 'D02', 'Engineer', 67000.00, NULL, 3),
    ('EMP008', 'Taylor', 'Robert', 'D01', 'Manager', 78000.00, 5500.00, 4);

 

-- Insert records into DEPT
INSERT INTO [dbo].[DEPT] ([dept_no], [dept_name], [mgn_no], [admr_dept], [location])
VALUES
    ('D01', 'Engineering', NULL, 'D01', 'Building A'),
    ('D02', 'Research', NULL, 'D01', 'Building B'),
    ('D03', 'Development', NULL, 'D01', 'Building C'),
    ('D04', 'Sales', NULL, 'D04', 'Building D'),
    ('D05', 'Marketing', NULL, 'D04', 'Building E');

 

-- Insert records into EMPPROJACT
INSERT INTO [dbo].[EMPPROJACT] ([emp_no], [proj_no], [act_no])
VALUES
    ('EMP001', 'PROJ01', 1),
    ('EMP001', 'PROJ02', 2),
    ('EMP002', 'PROJ01', 1),
    ('EMP003', 'PROJ03', 3),
    ('EMP003', 'PROJ02', 2),
    ('EMP004', 'PROJ04', 4),
    ('EMP005', 'PROJ05', 5),
    ('EMP006', 'PROJ03', 3);

 

-- Insert records into ACT
INSERT INTO [dbo].[ACT] ([act_no], [act_des])
VALUES
    (1, 'Design'),
    (2, 'Coding'),
    (3, 'Testing'),
    (4, 'Documentation'),
    (5, 'Review');

 

 

---Q2---Find employees who are currently working on a project or projects. Employees working on projects will have a row(s) on the EMPPROJACT table.
SELECT e.first_name, e.last_name, ep.proj_no
FROM EMP e
INNER JOIN EMPPROJACT ep ON ep.emp_no = e.emp_no

 

---Q3---Find all employees who major in math (MAT) and computer science (CSI).
SELECT e.first_name, e.last_name, em.major_name
FROM EMP e
INNER JOIN EMPMAJOR em ON em.emp_no = e.emp_no
WHERE em.major_name IN ('MAT','CSI')

 

 

---Q4---Find employees who work on all activities between 90 and 110
SELECT e.first_name, e.last_name, a.act_des
FROM EMPPROJACT ep
JOIN EMP e ON e.emp_no = ep.emp_no
JOIN ACT a ON a.act_no = ep.act_no
WHERE a.act_des BETWEEN 90 AND 110   --wrong syntax for requirement but its ok

 

 

---Q5---
WITH DepartmentAverage AS (
    SELECT dept_no, AVG(salary) AS DEPT_AVG_SAL
    FROM EMP
    GROUP BY dept_no
)
SELECT e.emp_no AS EMPNO, e.last_name AS LASTNAME, e.first_name AS FIRSTNAME, e.salary AS SALARY, e.dept_no AS DEPTNO, d.DEPT_AVG_SAL AS DEPT_AVG_SAL
FROM EMP e
LEFT JOIN DepartmentAverage d ON e.dept_no = d.dept_no;

 

 

---Q6---Use CTE technique to provide a report of employees whose education levels are higher than the average education level of their respective department.
WITH DepartmentEduAverage AS (
    SELECT dept_no, AVG(ed_level) AS EDU_AVG_LV
    FROM EMP
    GROUP BY dept_no
)
SELECT e.emp_no AS EMPNO, e.first_name AS FIRSTNAME, e.last_name AS LASTNAME, e.ed_level AS EDULEVEL, d.EDU_AVG_LV AS DEPT_AVG_EDULV
FROM EMP e
LEFT JOIN DepartmentEduAverage d ON d.dept_no = e.dept_no
WHERE e.ed_level > d.EDU_AVG_LV; --wrong syntax for requirement but its ok

 

 

---Q7---Return the department number, department name and the total payroll for the department that has the highest payroll. Payroll will be defined as the sum of all salaries and bonuses for the department.
WITH DepartmentPayroll AS (
    SELECT
        e.dept_no,
        SUM(e.salary + ISNULL(e.bonus, 0)) AS TotalPayroll
    FROM
        EMP e
    GROUP BY
        e.dept_no
)

 

SELECT d.dept_no, d.dept_name, dp.TotalPayroll
FROM
    DEPT d
JOIN (
    SELECT dept_no, MAX(TotalPayroll) AS HighestPayroll
    FROM DepartmentPayroll
    GROUP BY dept_no
) AS MaxPayroll ON d.dept_no = MaxPayroll.dept_no
JOIN DepartmentPayroll dp ON d.dept_no = dp.dept_no
WHERE dp.TotalPayroll = MaxPayroll.HighestPayroll;

 

 

---Q8---Return the employees with the top 5 salaries.

 

--------Could be 5 employees with different salaries.

 

--------Could be many employees having the same salaries.

 

SELECT TOP 5 e.emp_no, e.first_name, e.last_name, e.salary
FROM EMP e
ORDER BY e.salary DESC