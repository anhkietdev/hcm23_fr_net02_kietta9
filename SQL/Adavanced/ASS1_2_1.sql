-- Create the San_Pham table
CREATE TABLE San_Pham (
    Ma_SP INT PRIMARY KEY,
    Ten_SP VARCHAR(255) NOT NULL,
    Don_Gia DECIMAL(10, 2) NOT NULL
);

 

-- Create the Khach_Hang table
CREATE TABLE Khach_Hang (
    Ma_KH INT PRIMARY KEY,
    Ten_KH VARCHAR(255) NOT NULL,
    Phone_No VARCHAR(20),
    Ghi_Chu VARCHAR(255)
);

 

-- Create the Don_Hang table with foreign keys
CREATE TABLE Don_Hang (
    Ma_DH INT PRIMARY KEY,
    Ngay_DH DATE NOT NULL,
    Ma_SP INT,
    So_Luong INT NOT NULL,
    Ma_KH INT,
    FOREIGN KEY (Ma_SP) REFERENCES San_Pham(Ma_SP),
    FOREIGN KEY (Ma_KH) REFERENCES Khach_Hang(Ma_KH)
);

 

 

-- Insert data into the San_Pham table
INSERT INTO San_Pham (Ma_SP, Ten_SP, Don_Gia)
VALUES
    (1, 'Product A', 50.00),
    (2, 'Product B', 75.00),
    (3, 'Product C', 30.00);

 

-- Insert data into the Khach_Hang table
INSERT INTO Khach_Hang (Ma_KH, Ten_KH, Phone_No, Ghi_Chu)
VALUES
    (1, 'Customer 1', '123-456-7890', 'Regular customer'),
    (2, 'Customer 2', '987-654-3210', 'VIP customer'),
    (3, 'Customer 3', '555-555-5555', NULL);

 

-- Insert data into the Don_Hang table
INSERT INTO Don_Hang (Ma_DH, Ngay_DH, Ma_SP, So_Luong, Ma_KH)
VALUES
    (1, '2023-09-01', 1, 5, 1),
    (2, '2023-09-02', 2, 3, 2),
    (3, '2023-09-03', 3, 2, 3);

 

 

	--2--
CREATE VIEW REQUIRE AS
SELECT k.Ten_KH, d.Ngay_DH, s.Ten_SP, d.So_Luong, (s.Don_Gia * d.So_Luong) AS Thanh_Tien
FROM Don_Hang d
INNER JOIN Khach_Hang k ON k.Ma_KH = d.Ma_KH
INNER JOIN San_Pham s ON s.Ma_SP = d.Ma_SP 